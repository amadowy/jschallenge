﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace JSChallenge.Migrations
{
    public partial class SeedingDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("INSERT INTO Skills (SkillName) VALUES ('Umiejetnosc1')");
            migrationBuilder.Sql("INSERT INTO Skills (SkillName) VALUES ('Umiejetnosc2')");

            migrationBuilder.Sql("INSERT INTO Users (Login, Password, CreateDate, Name, Surname, [Desc], SkillId) VALUES ('amad94', 'amad123', '20180322 05:50:10 PM', 'Amadeusz', 'Snarski', 'Opis1', 1)");
            migrationBuilder.Sql("INSERT INTO Users (Login, Password, CreateDate, Name, Surname, [Desc], SkillId) VALUES ('arek159', 'arek123', '20180322 06:20:34 PM', 'Arkadiusz', 'Michalski', 'Opis2', 2)");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DELETE FROM Skills");
            migrationBuilder.Sql("DELETE FROM Users");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JSChallenge.Models;
using JSChallenge.Models.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace JSChallenge.Controllers
{
    [Produces("application/json")]
    [Route("api/User")]
    public class UserController : Controller
    {
        private readonly IUserRepo _userRepo;
        private readonly ISkillRepo _skillRepo;

        public UserController(IUserRepo userRepo, ISkillRepo skillRepo)
        {
            _userRepo = userRepo;
            _skillRepo = skillRepo;
        }

        [HttpGet]
        [Route("getAllUsers")]
        public IActionResult GetAllUsers()
        {
            return new JsonResult(_userRepo.GetAll());
        }

        [HttpPost]
        [Route("addUser")]
        public IActionResult AddUser([FromBody] User user)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var skill = _skillRepo.GetSkillById(user.SkillId);
            if(skill == null)
            {
                return NotFound("Cannot find skill with provided skillId");
            }

            _userRepo.AddUser(user, skill);
            return new JsonResult(user.UserId);
        }

        [HttpGet]
        [Route("getUserById/{userId}")]
        public IActionResult GetUserById(int userId)
        {
            if(userId <= 0)
            {
                return BadRequest("Id cannot be less than 0");
            }

            return new JsonResult(_userRepo.GetUserById(userId));
        }

        [HttpPost]
        [Route("updateUser")]
        public IActionResult UpdateUser([FromBody] User user)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _userRepo.UpdateUser(user);
            return new JsonResult(user.UserId);
        }

        [HttpGet]
        [Route("delete/{userId}")]
        public IActionResult DeleteUser(int userId)
        {
            if(userId <= 0)
            {
                return BadRequest("Id cannot be less than 0.");
            }

            var user = _userRepo.GetUserById(userId);
            if(user == null)
            {
                return NotFound("Cannot find user with provided userId");
            }

            var skill = _skillRepo.GetSkillById(user.SkillId);
            if(skill == null)
            {
                return NotFound("Cannot find user with provided skillId");
            }

            _userRepo.DeleteUser(user, skill);
            return new JsonResult(user.UserId);
        }

        
    }
}
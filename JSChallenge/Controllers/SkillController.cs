﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JSChallenge.Models;
using JSChallenge.Models.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace JSChallenge.Controllers
{
    [Produces("application/json")]
    [Route("api/Skill")]
    public class SkillController : Controller
    {
        private readonly ISkillRepo _skillRepo;

        public SkillController(ISkillRepo skillRepo)
        {
            _skillRepo = skillRepo;
        }

        [HttpPost]
        [Route("addSkill")]
        public IActionResult AddSkill([FromBody] Skill skill)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _skillRepo.AddSkill(skill);
            return new JsonResult(skill.SkillId);
        }
    }
}
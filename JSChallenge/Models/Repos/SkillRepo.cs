﻿using JSChallenge.Models.Database;
using JSChallenge.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JSChallenge.Models.Repos
{
    public class SkillRepo : ISkillRepo
    {
        private readonly DatabaseContext _databaseContext;

        public SkillRepo(DatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public int AddSkill(Skill skill)
        {
            if(skill == null)
            {
                throw new Exception("Skill object cannot be null");
            }

            _databaseContext.Skills.Add(skill);
            _databaseContext.SaveChanges();
            return skill.SkillId;
        }

        public Skill GetSkillById(int skillId)
        {
            if(skillId <= 0)
            {
                throw new Exception("Id cannot be less than 0");
            }

            return _databaseContext.Skills.FirstOrDefault(skill => skill.SkillId == skillId);
        }
    }
}

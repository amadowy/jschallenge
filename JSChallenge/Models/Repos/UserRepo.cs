﻿using JSChallenge.Models.Database;
using JSChallenge.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JSChallenge.Models.Repos
{
    public class UserRepo : IUserRepo
    {
        private readonly DatabaseContext _databaseContext;

        public UserRepo(DatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public int AddUser(User user, Skill skill)
        {
            if (user == null || skill == null)
            {
                throw new Exception("User object and Skill object cannot be null");
            }

            user.UserId = 0;

            user.Skills = skill;
            user.SkillId = skill.SkillId;

            _databaseContext.Users.Add(user);
            _databaseContext.SaveChanges();
            return user.UserId;
        }

        public void DeleteUser(User user, Skill skill)
        {
            if (user == null || skill == null)
            {
                throw new Exception("User object and Skill object cannot be null");
            }

            _databaseContext.Users.Remove(user);
            _databaseContext.SaveChanges();

            _databaseContext.Skills.Remove(skill);
            _databaseContext.SaveChanges();
        }

        public List<User> GetAll()
        {
            return _databaseContext.Users.ToList();
        }

        public User GetUserById(int userId)
        {
            return _databaseContext.Users.Where(user => user.UserId == userId).FirstOrDefault();
        }

        public int UpdateUser(User user)
        {
            if(user == null)
            {
                throw new Exception("User object cannot be null");
            }

            _databaseContext.Users.Update(user);
            _databaseContext.SaveChanges();
            return user.UserId;
        }
    }
}

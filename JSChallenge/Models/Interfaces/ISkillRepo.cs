﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JSChallenge.Models.Interfaces
{
    public interface ISkillRepo
    {
        int AddSkill(Skill skill);
        Skill GetSkillById(int SkillId);
    }
}

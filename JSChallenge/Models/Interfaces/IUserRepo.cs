﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JSChallenge.Models.Interfaces
{
    public interface IUserRepo
    {
        List<User> GetAll();
        User GetUserById(int userId);
        int AddUser(User User, Skill Skill);
        int UpdateUser(User user);
        void DeleteUser(User User, Skill Skill);
    }
}

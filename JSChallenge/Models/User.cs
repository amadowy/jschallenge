﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JSChallenge.Models
{
    public class User
    {
        public int UserId { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public DateTime CreateDate { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Desc { get; set; }

        public virtual int SkillId { get; set; }
        public virtual Skill Skills { get; set; }
    }
}

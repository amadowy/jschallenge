﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JSChallenge.Models
{
    public class Skill
    {
        public int SkillId { get; set; }
        public string SkillName { get; set; }
    }
}
